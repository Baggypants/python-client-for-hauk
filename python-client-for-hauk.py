import requests
import time 
import os
import sys
import dbus
#import gi
#gi.require_version('Geoclue','2.0')
#from gi.repository import Geoclue
import pyperclip 
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GObject
import pdb

# geoclue from https://github.com/atareao/GeoClue2-Locator-python/

system_bus = None

GEOCLUE2_BUS_NAME = 'org.freedesktop.GeoClue2'
MANAGER_INTERFACE_NAME = GEOCLUE2_BUS_NAME + '.Manager'
CLIENT_INTERFACE_NAME = GEOCLUE2_BUS_NAME + '.Client'
LOCATION_INTERFACE_NAME = GEOCLUE2_BUS_NAME + '.Location'
PROPERTIES_INTERFACE_NAME = 'org.freedesktop.DBus.Properties'


def get_object(bus, name, object_path):
    '''
    This function returns a dbus proxy object located at the path
    provided in object_path
    '''
    try:
        return bus.get_object(name, object_path)
    except dbus.DBusException:
        traceback.print_exc()
        sys.exit(1)

def get_interface(proxy_object, interface_name):
    '''
    This function takes a proxy object and returns the interface whose name
    is provided in interface_name
    '''
    try:
        return dbus.Interface(proxy_object, interface_name)
    except dbus.DBusException:
        traceback.print_exc()
        sys.exit(1)

def get_property(properties_interface, interface_name, property_name):
    '''
    This function gets the value of a property. Parameters are -
    the property interface, interface name where the property is located,
    and the name of the property to be fetched
    '''
    try:
        return properties_interface.Get(interface_name, property_name)
    except dbus.DBusException:
        traceback.print_exc()
        sys.exit(1)

def set_property(properties_interface, interface_name, property_name, value):
    '''
    This function gets the value of a property. Parameters are -
    the property interface, interface name where the property is located,
    the name of the property to be fetched and the value to be set
    '''
    try:
        return properties_interface.Set(interface_name, property_name, value)
    except dbus.DBusException:
        traceback.print_exc()
        sys.exit(1)

def location_updated(old_path, new_path):
    '''
    When 'LocationUpdated' signal gets emitted, this function gets called
    It fetches the location object located at the new path, and prints the
    location details
    '''
    location_object = get_object(system_bus, GEOCLUE2_BUS_NAME, new_path)
    location_properties = get_interface(location_object,
                               PROPERTIES_INTERFACE_NAME)
    hauklat = get_property(location_properties, LOCATION_INTERFACE_NAME,
                            'Latitude')
    hauklon = get_property(location_properties, LOCATION_INTERFACE_NAME,
                            'Longitude')
    haukacc = get_property(location_properties, LOCATION_INTERFACE_NAME,
                            'Accuracy')
    description = get_property(location_properties, LOCATION_INTERFACE_NAME,
                            'Description')
    haukspeed = get_property(location_properties, LOCATION_INTERFACE_NAME,
                            'Speed')

    nowish = str(int(time.time()))
    hauklocationupdate = requests.post(haukserverurl + '/api/post.php',
        data={
            "spd":haukspeed,
            "acc":haukacc,
            "prv":"0",
            "lat": hauklat,
            "lon": hauklon,
            "time": nowish, 
            "sid": hauksessionkey
            },
        headers = haukheader
        )
    print(str(hauklat),str(hauklon))

def main():

    global system_bus
    # In order to make asynchronous calls, we need to setup an event loop
    # Go ahead and try removing this, the 'connect_to_signal' method
    # at the bottom won't work
    dbus_loop = DBusGMainLoop(set_as_default = True)

    # We connect to the system bus as GeoClue2 is located there
    system_bus = dbus.SystemBus(mainloop = dbus_loop)

    # We get the proxy object and then the interface
    manager_object = get_object(system_bus, GEOCLUE2_BUS_NAME,
                                '/org/freedesktop/GeoClue2/Manager')
    manager = get_interface(manager_object, MANAGER_INTERFACE_NAME)

    # GetClient() returns the path to the newly created client object
    client_path = manager.GetClient()
    client_object = get_object(system_bus, GEOCLUE2_BUS_NAME, client_path)

    # We set the 'DistanceThreshold' property before starting client.
    # This property decides how often 'LocationUpdated' signal is emitted
    # If the distance moved is below threshold, the signal won't be emitted
    # We have set this to 0 meters
    client_properties = get_interface(client_object, PROPERTIES_INTERFACE_NAME)
    set_property(client_properties, CLIENT_INTERFACE_NAME,
                 'DistanceThreshold', dbus.UInt32(0))
    set_property(client_properties, CLIENT_INTERFACE_NAME,
                 'DesktopId', 'Python-Client-For-Hauk')
    set_property(client_properties, CLIENT_INTERFACE_NAME,
                 'RequestedAccuracyLevel',dbus.UInt32(8))  #EXACT

    client = get_interface(client_object, CLIENT_INTERFACE_NAME)

    # Connect to the 'LocationUpdated' signal before starting the client
    client.connect_to_signal('LocationUpdated', location_updated)

    # Start receiving events about current location
    client.Start()
    
    loop = GObject.MainLoop()
    GObject.timeout_add(int(haukduration)*1000, loop.quit)
    loop.run()
    client.Stop()



if not "HAUK_USER" in os.environ:
    print("you need to set environment vars")
    exit()

haukserverurl = os.getenv("HAUK_SERVER_URL")
haukusername = os.getenv("HAUK_USER")
haukpassword = os.getenv("HAUK_PASSWD")
haukduration = os.getenv("HAUK_DURATION", default="1800")
haukinterval = "1"
haukadoptable = "1"
hauksharemode = "0" 
haukheader = {"Content-Type": "application/x-www-form-urlencoded"}


#clue = Geoclue.Simple.new_sync('something',Geoclue.AccuracyLevel.EXACT,None)
#clue = Geoclue.ClientProxy.create_sync('something',Geoclue.AccuracyLevel.EXACT,None)
# example data dur=1800&mod=0&usr=Leon&e2e=0&pwd=572813&ado=1&int=1
#pdb.set_trace()
haukinitiate = requests.post(haukserverurl + '/api/create.php', 
        data={ 
            "dur": haukduration,
            "int": haukinterval, 
            "ado": haukadoptable, 
            "usr": haukusername,
            "pwd": haukpassword,
            "mod": hauksharemode
            },
        headers = haukheader
        )

if 'ncorrect password' in haukinitiate.text:
    print(haukinitiate.text)
    exit()
hauklogin = []
for i in haukinitiate.iter_lines(): 
    hauklogin.append(i)

hauksessionkey = hauklogin[-3]
haukurl = hauklogin[-2].decode('utf-8')

# example time post acc=8.4&prv=0&spd=0.0&lon=-2.3083233333333335&time=1.658228667458E9&lat=53.48522666666666&sid=50fc945a170607620652e8abff945d7d
# comparator lat=53.487522&lon=-2.290126&time=1658228945.1807501&sid=6bd36632b6e822d37a98fa6f95e3e0b64746bac7d9c17a2fbf98fb6d25762a82
pyperclip.set_clipboard('wl-clipboard')
pyperclip.copy(haukurl)
print(haukurl + " has been copied to the clipboard")


main()




