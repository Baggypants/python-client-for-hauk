# Python Client for Hauk

A simple proof of concept using python to relay location data via the HAUK service. Tested on Mobian and Fedora

## Setup

Install your distributions copy of `gir1.2-geoclue-2.0`

create a venv and also specify system modules
 
```bash
python3 -m venv venv --system-site-packages
source venv/bin/activate
pip3 install -r requirements.txt
```

setup the env vars to your needed credentials

```bash
export HAUK_USER="username"
export HAUK_PASSWD="password"
export HAUK_SERVER_URL="https://haukserver/path/"
```

run the pyfile

`python3 python-client-for-hauk.py`

Share the resulting link


